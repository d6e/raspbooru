#![feature(proc_macro_hygiene, decl_macro)]

#[macro_use] extern crate diesel;
#[macro_use] extern crate rocket;
// #[macro_use] extern crate rocket_contrib;
#[macro_use] extern crate serde_derive;
// #[macro_use] extern crate serde;

mod db;

// use diesel::r2d2::Pool;
// use diesel::r2d2::ConnectionManager;
use diesel::Connection;
// use diesel::prelude::*;
use rocket::request::{FlashMessage};
use rocket_contrib::{templates::Template, serve::StaticFiles};
use rocket_contrib::databases::diesel::SqliteConnection;
use dotenv::dotenv;
use std::env;

pub fn establish_connection() -> SqliteConnection {
    dotenv().ok();

    let database_url = env::var("DATABASE_URL")
        .expect("DATABASE_URL must be set");
    SqliteConnection::establish(&database_url)
        .expect(&format!("Error connecting to {}", database_url))
}

// pub fn create_db_pool() -> Pool<ConnectionManager<SqliteConnection>> {
//     dotenv().ok();
//
//     let database_url = env::var("DATABASE_URL")
//         .expect("DATABASE_URL must be set");
//     let config = Config::default();
//     let manager = ConnectionManager::<SqliteConnection>::new(database_url);
//     Pool::new(config, manager).expect("Failed to create pool.")
// }

// #[derive(Debug, Serialize)]
// struct Context<'a, 'b>{ msg: Option<(&'a str, &'b str)>, tasks: Vec<Task> }
//
// impl<'a, 'b> Context<'a, 'b> {
//     pub fn err(conn: &DbConn, msg: &'a str) -> Context<'static, 'a> {
//         Context{msg: Some(("error", msg)), tasks: Task::all(conn)}
//     }
//
//     pub fn raw(conn: &DbConn, msg: Option<(&'a str, &'b str)>) -> Context<'a, 'b> {
//         Context{msg: msg, tasks: Task::all(conn)}
//     }
// }

#[derive(Debug, Serialize)]
struct Context{
    site_logo: Option<String>,
    site_name: Option<String>,
    images: Option<Vec<String>>,
}

#[get("/")]
fn index(msg: Option<FlashMessage>) -> Template {
// fn index(msg: Option<FlashMessage>, conn: DbConn) -> Template {
    // Template::render("index", &match msg {
    //     Some(ref msg) => Context::raw(&conn, Some((msg.name(), msg.msg()))),
    //     None => Context::raw(&conn, None),
    // })
    // let imgs = vec![
    //     String::from("https://bulma.io/images/placeholders/640x480.png"),
    //     String::from("https://bulma.io/images/placeholders/640x480.png"),
    //     String::from("https://bulma.io/images/placeholders/640x480.png"),
    //     String::from("https://bulma.io/images/placeholders/640x480.png"),
    //     String::from("https://bulma.io/images/placeholders/640x480.png"),
    //     String::from("https://bulma.io/images/placeholders/640x480.png"),
    //     String::from("https://bulma.io/images/placeholders/640x480.png"),
    //     String::from("https://bulma.io/images/placeholders/640x480.png"),
    //     String::from("https://bulma.io/images/placeholders/640x480.png"),
    //     String::from("https://bulma.io/images/placeholders/640x480.png"),
    // ];
    let connection = establish_connection();
    let image = db::models::InsertableImage {
        name: String::from("trans inkling"),
        url: String::from("static/img/4a40b0a27caab45f.png"),
    };
    let result = db::queries::insert(image, &connection);
    if result.is_err() {
        println!("Error inserting: {:?}", result.unwrap_err())
    }
    let all_images = db::queries::all(&connection);
    match all_images {
        Err(e) => {
            println!("Error fetching all: {:?}", e);
            let context = Context {
                site_logo: Option::from(String::from("static/img/logo.jpg")),
                site_name: Option::from(String::from("Raspbooru")),
                images: Option::from(Vec::new()),
            };
            Template::render("index", &context)
        },
        Ok(images)  => {
            let img_urls = images.iter().map(|x| x.url.clone()).collect::<Vec<String>>();
            let context = Context {
                site_logo: Option::from(String::from("static/img/logo.jpg")),
                site_name: Option::from(String::from("Raspbooru")),
                images: Option::from(img_urls),
            };
            Template::render("index", &context)
        }
    }


}

fn main() {
    rocket::ignite()
    .attach(Template::fairing())
    .mount("/static", StaticFiles::from("static/"))
    .mount("/", routes![index])
    .launch();
}
