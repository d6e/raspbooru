table! {
    images (id) {
        id -> Integer,
        name -> Text,
        url -> Text,
    }
}
