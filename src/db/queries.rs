use diesel;
use diesel::prelude::*;
use super::schema::images;
use super::models::{Image, InsertableImage};

pub fn all(connection: &SqliteConnection) -> QueryResult<Vec<Image>> {
    images::table.load::<Image>(&*connection)
}

pub fn get(id: i32, connection: &SqliteConnection) -> QueryResult<Image> {
    images::table.find(id).get_result::<Image>(connection)
}

pub fn insert(image: InsertableImage, connection: &SqliteConnection) -> QueryResult<usize> {
    diesel::insert_into(images::table)
        .values(&image)
        .execute(connection)
}

pub fn update(id: i32, image: Image, connection: &SqliteConnection) -> QueryResult<usize> {
    diesel::update(images::table.find(id))
        .set(&image)
        .execute(connection)
}

pub fn delete(id: i32, connection: &SqliteConnection) -> QueryResult<usize> {
    diesel::delete(images::table.find(id))
        .execute(connection)
}
