use diesel::deserialize::Queryable;
use diesel::prelude::*;
use super::schema::images;

#[derive(Queryable, AsChangeset, Serialize, Deserialize)]
#[table_name = "images"]
pub struct Image {
    pub id: i32,
    pub name: String,
    pub url: String,
}

#[derive(Insertable)]
#[table_name = "images"]
pub struct InsertableImage {
    pub name: String,
    pub url: String,
}

impl InsertableImage {
    pub fn from_image(image: Image) -> InsertableImage {
        InsertableImage {
            name: image.name,
            url: image.url,
        }
    }
}
