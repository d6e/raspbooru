-- Your SQL goes here
CREATE TABLE images (
  id INTEGER PRIMARY KEY,
  name VARCHAR NOT NULL,
  url TEXT NOT NULL
)
